<?php

namespace App\Controller;

use App\Core\ControllerBase;
use App\Model\UserResult;
/**
 * Description of Results
 *
 * @author kalvis
 */
class ResultsController extends ControllerBase {

    public $user_answer;

    public function __construct() {
        $results_allowed = isset($_SESSION['printquiz.user']['id']) && isset($_SESSION['printquiz.quiz']['id']);
        if (!$results_allowed) {
            header("Location: " . "/");
        }
        $this->user_answer = new UserResult(
                $_SESSION['printquiz.user']['id'], $_SESSION['printquiz.quiz']['id']
        );
        $this->user_answer->load();
        parent::__construct(); //TWIG INI
    }

    //index view
    public function index() {
        $data = [];
        $data['results'] = $this->user_answer->push();
        unset($_SESSION['printquiz.user']);
        unset($_SESSION['printquiz.quiz']);
        unset($_SESSION['printquiz.questions']);
        unset($_SESSION['printquiz.active_question']);
        $response = [
            "page" => $this->view('containers/results-container.twig', $data)
        ];
        header('Content-Type: application/json');
        echo json_encode($response);
        die();
    }

}
