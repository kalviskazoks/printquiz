<?php

namespace App\Controller;

use App\Core\ControllerBase;
use App\Model\User;
use App\Model\Quiz;
use App\Model\Question;
use App\Model\UserAnswer;

/**
 * Description of Quiz
 *
 * @author kalvis
 * Quiz controller
 */
class QuizController extends ControllerBase {

    public $user;
    public $quiz;
    public $question;
    public $questions;
    public $active_question;

    public function __construct() {
        //if no user nor quiz is registered on SESSION -> redirect to home page
        $quiz_allowed = isset($_SESSION['printquiz.user']) && isset($_SESSION['printquiz.quiz']);
        if (!$quiz_allowed) {
            header('Location: ' . '/home');
        }

        $this->user = new User();
        $this->user->load($_SESSION['printquiz.user']['id']);

        $this->quiz = new Quiz();
        $this->quiz->load($_SESSION['printquiz.quiz']['id']);

        $this->questions = $this->quiz->loadQuizQuestionIds();

        if (!isset($_SESSION['printquiz.active_question'])) {
            $_SESSION['printquiz.active_question'] = 0;
        }
        $this->active_question = $_SESSION['printquiz.active_question'];

        $this->question = new Question();
        $this->question->load($this->questions[$this->active_question]->id);
        parent::__construct(); //TWIG init
    }

    public function index() {
        $response = [];

        $data = [];
        $data['question'] = $this->question->getText();

        $data['button_text'] = $this->active_question == count($this->questions) - 1 ? "Finish" : "Next";

        $data['form'] = [
            'answer' => []
        ];
        $data['form']['answer']['options'] = $this->question->loadAnswers();

        $progress = ($this->active_question + 1) * 100 / count($this->questions);
        //round down to not get more than 100%
        $data['progress'] = round($progress, 0, PHP_ROUND_HALF_DOWN);

        $request_method = $_SERVER['REQUEST_METHOD'];
        if ($request_method == 'POST') {
            //default values
            $data['form']['answer']['value'] = (int) $_POST["answer"];
            if ($data['form']['answer']['value'] > 0) {

                //register answer
                $user_answer = new UserAnswer(
                        $this->user->getId(), $this->quiz->getId(), $this->question->getId(), $data['form']['answer']['value']
                );
                $user_answer->create();

                $next_question = $this->active_question + 1;

                if ($next_question < count($this->questions)) {
                    //if there are more questions - update session and redirect back to quiz
                    $_SESSION['printquiz.active_question'] = $next_question;

                    //$data = [];
                    $response = [
                        'next' => false,
                        'get' => true,
                    ];
                    //header('Location: ' . '/quiz');
                } else {
                    //if there are no more questions - open the results page
                    $response = [
                        "next" => true,
                        "get" => false,
                    ];
                }
            } else {
                $data['form']['answer']['errors'][] = "Please choose an answer!";
                $response = [
                    'next' => false,
                    'form' => $this->view("form/quiz-form.twig", $data),
                ];
            }
        } else {
            $response['container'] = $this->view('containers/quiz-container.twig', $data);
        }
        header('Content-Type: application/json');
        echo json_encode($response);
        die();
    }

}
