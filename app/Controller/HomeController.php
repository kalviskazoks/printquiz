<?php

namespace App\Controller;

use App\Core\ControllerBase;
use App\Core\Database\Driver\Mysql;
use App\Model\User;
use App\Model\Quiz;

/**
 * Description of Home
 *
 * @author kalvis
 * Home controller
 */
class HomeController extends ControllerBase {

    public function __construct() {
        $database = new Mysql();
        parent::__construct(); //TWIG init
    }

    public function index() {
        $data = [];
        $quiz = new Quiz();
        $data['quizes'] = $quiz->loadAll();
        //initialize form variables
        $data['form'] = [
            'name' => [],
            'quiz' => [],
        ];

        $request_method = $_SERVER['REQUEST_METHOD'];
        if ($request_method == 'POST') {
            $response = [];

            //default values
            $name = isset($_POST['name']) ? $_POST['name'] : "";
            $quiz_id = isset($_POST['quiz']) ? $_POST['quiz'] : 0;
            $data['form']['name']['value'] = trim($name);
            $data['form']['quiz']['value'] = (int) $quiz_id;

            //server side validation
            $form_valid = true;
            if (empty($data['form']['name']['value'])) {
                $data['form']['name']['errors'][] = "Please enter your name!";
                $form_valid = false;
            }

            if ($data['form']['quiz']['value'] == 0) {
                $data['form']['quiz']['errors'][] = "Please choose a test!";
                $form_valid = false;
            }
            if ($form_valid) {

                $user = new User($name);
                $user->create();
                $user->setSession();

                $quiz->load($quiz_id);
                $quiz->setSession();

                $response = [
                    "next" => true,
                    "next_url" => "/quiz",
                ];
            } else {
                $response = [
                    'next' => false,
                    'form' => $this->view("form/home-form.twig", $data),
                ];
            }
            if (!empty($response)) {
                header('Content-Type: application/json');
                echo json_encode($response);
                die();
            }
        }
        echo $this->view("pages/home-page.twig", $data);
    }
}
