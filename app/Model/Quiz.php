<?php

namespace App\Model;

use App\Core\ModelBase;
use App\Core\Database\Driver\Mysql;

/**
 * Description of Quiz
 *
 * @author kalvis
 */
class Quiz extends ModelBase {

    private $db;
    public $name;
    public $open;
    public $questions;
    public $active_question;

    public function __construct() {
        parent::__construct();
        $this->db = new Mysql();
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getName() {
        return $this->name;
    }

    public function setOpen($open) {
        $this->open = $open;
    }

    public function getOpen() {
        return $this->open;
    }

    public function load($quiz_id) {
        $sql = "SELECT * FROM quiz WHERE id = " . (int) $quiz_id;
        $result = $this->db->fetch($sql);
        if (!empty($result)) {
            $this->setId($result->id);
            $this->setName($result->name);
            $this->setOpen($result->open);
        }
    }

    public function loadAll($open = true) {
        $sql = "SELECT * FROM quiz WHERE open = " . (int) $open;
        $result = $this->db->fetchAll($sql);
        return $result;
    }

    public function loadQuizQuestions() {
        $sql = "SELECT * FROM question WHERE _quiz_id = " . (int) $this->getId() . " ORDER BY sort_order ASC";
        $result = $this->db->fetchAll($sql);
        return $result;
    }
    
    public function loadQuizQuestionIds() {
        $sql = "SELECT id FROM question WHERE _quiz_id = " . $this->getId() . " ORDER BY sort_order ASC";
        $data = $this->db->fetchAll($sql);
        return $data;
    }

    public function readSession() {
        if (isset($_SESSION['printquiz.quiz'])) {
            return $_SESSION['printquiz.quiz'];
        }
    }

    public function setSession() {
        $_SESSION['printquiz.quiz'] = [
            "id" => $this->getId(),
            "name" => $this->getName(),
            "open" => $this->getOpen(),
        ];
    }

    public function unsetSession() {
        if (isset($_SESSION['printquiz.quiz'])) {
            unset($_SESSION['printquiz.quiz']);
        }
    }

}
