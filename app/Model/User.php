<?php

namespace App\Model;

use App\Core\ModelBase;
use App\Core\Database\Driver\Mysql;

/**
 * Description of User
 *
 * @author kalvis
 */
class User extends ModelBase {

    private $db;
    public $name;
    public $hash;

    public function __construct($name = null) {
        parent::__construct();
        $this->setName($name);
        $this->db = new Mysql();
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getName() {
        return $this->name;
    }

    public function setHash($hash) {
        $this->hash = $hash;
    }

    public function getHash() {
        return $this->hash;
    }

    public function create() {
        $this->setHash(session_id() . '_' . time());
        $user_data = [
            'name' => $this->getName(),
            'session_hash' => $this->getHash()
        ];
        $this->setId($this->db->insert("user", $user_data));
    }

    public function load($user_id) {
        $sql = "SELECT * FROM user WHERE id = " . (int) $user_id;
        $result = $this->db->fetch($sql);
        if (!empty($result)) {
            $this->setId($result->id);
            $this->setName($result->name);
            $this->setHash($result->session_hash);
        }
    }

    public function readSession() {
        if (isset($_SESSION['printquiz.user'])) {
            return $_SESSION['printquiz.user'];
        }
        return null;
    }

    public function setSession() {
        $_SESSION['printquiz.user'] = [
            "id" => $this->getId(),
            "name" => $this->getName(),
        ];
    }

    public function unsetSession() {
        if (isset($_SESSION['printquiz.user'])) {
            unset($_SESSION['printquiz.user']);
        }
    }

}
