<?php

namespace App\Model;

use App\Core\ModelBase;
use App\Core\Database\Driver\Mysql;

/**
 * Description of UserAnswer
 *
 * @author kalvis
 */
class UserAnswer extends ModelBase {

    private $db;
    public $user_id;
    public $quiz_id;
    public $question_id;
    public $answer_id;

    public function __construct($user_id, $quiz_id, $question_id, $answer_id) {
        parent::__construct();
        $this->user_id = $user_id;
        $this->quiz_id = $quiz_id;
        $this->question_id = $question_id;
        $this->answer_id = $answer_id;
        $this->db = new Mysql();
    }

    public function create() {
        $user_answer_data = [
            '_user_id' => $this->user_id,
            '_quiz_id' => $this->quiz_id,
            '_question_id' => $this->question_id,
            '_answer_id' => $this->answer_id,
        ];
        $this->setId($this->db->insert("user_answer", $user_answer_data));
    }

}
