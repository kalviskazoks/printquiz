<?php

namespace App\Model;

use App\Core\ModelBase;
use App\Core\Database\Driver\Mysql;

/**
 * Description of UserResult
 *
 * @author kalvis
 */
class UserResult extends ModelBase {

    private $db;
    public $user_id;
    public $user_name;
    public $quiz_id;
    public $total_answers;
    public $correct_answers;

    public function __construct($user_id, $quiz_id) {
        parent::__construct();
        $this->user_id = $user_id;
        $this->quiz_id = $quiz_id;
        $this->db = new Mysql();
    }

    public function load() {
        $sql = "SELECT * FROM v_quiz_results WHERE user_id = " . (int) $this->user_id . " AND quiz_id = " . $this->quiz_id;
        $data = $this->db->fetch($sql);
        if ($data) {
            $this->user_name = $data->user_name;
            $this->total_answers = $data->total_answers;
            $this->correct_answers = $data->correct_answers;
        }
    }

    public function push() {
        return[
            "user_name" => $this->user_name,
            "total_answers" => $this->total_answers,
            "correct_answers" => $this->correct_answers
        ];
    }

}
