<?php

namespace App\Model;

use App\Core\ModelBase;
use App\Core\Database\Driver\Mysql;

/**
 * Description of Quiz
 *
 * @author kalvis
 */
class Question extends ModelBase {

    private $db;
    public $quiz;
    public $text;
    public $sort_order;

    public function __construct() {
        parent::__construct();
        $this->db = new Mysql();
    }

    public function setQuiz($quiz) {
        $this->quiz = $quiz;
    }

    public function getQuiz() {
        return $this->quiz;
    }

    public function setText($text) {
        $this->open = $text;
    }

    public function getText() {
        return $this->text;
    }

    public function setSortOrder($sort_order) {
        $this->sort_order = $sort_order;
    }

    public function getSortOrder() {
        return $this->sort_order;
    }

    public function load($quest_id) {
        $sql = "SELECT * FROM question WHERE id = " . (int) $quest_id;
        $result = $this->db->fetch($sql);
        if (!empty($result)) {
            $this->setId($result->id);
            $this->setText($result->text);
            $this->setQuiz($result->_quiz_id);
            $this->setSortOrder($result->sort_order);
        }
    }
    
    public function loadAnswers() {
        $sql = "SELECT * FROM answer WHERE _question_id = " . (int) $this->getId();
        $data = $this->db->fetchAll($sql);
        return $data;
    }

    public function readSession() {
        if (isset($_SESSION['printquiz.question'])) {
            return $_SESSION['printquiz.question'];
        }
    }

    public function setSession() {
        $_SESSION['printquiz.question'] = [
            "id" => $this->getId(),
            "_quiz_id" => $this->getQuiz(),
            "text" => $this->getText(),
            "sort_order" => $this->getSortOrder()
        ];
    }

    public function unsetSession() {
        if (isset($_SESSION['printquiz.question'])) {
            unset($_SESSION['printquiz.question']);
        }
    }

}
