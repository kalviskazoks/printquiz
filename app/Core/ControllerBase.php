<?php

namespace App\Core;

use Twig_Environment;
use Twig_Loader_Filesystem;
use Twig_Extension_Debug;

/**
 * Description of Controller
 *
 * @author kalvis
 */
class ControllerBase {

    public $renderer;

    public function __construct() {
        $loader = new Twig_Loader_Filesystem("templates");
        $this->renderer = new Twig_Environment($loader, [
            'debug' => true,
            'strict_variables' => true,
        ]);
        $this->renderer->addExtension(new Twig_Extension_Debug());
    }

    public function index() {
        echo "Index!";
    }

    public function model($model) {
//        require_once '../app/model/' . $model . '.php';
//        return new $model();
        return "Model!";
    }

    public function view($template = "layout/page.twig", $data = []) {
        return $this->renderer->render($template, $data);
    }
}
