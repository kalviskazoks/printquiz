<?php

namespace App\Core;

/**
 * Description of App
 *
 * @author kalvis
 * Provides basic routing mechanism and MVC principles
 */
class App {

    //default controller
    protected $controller = "HomeController";
    //default method
    protected $method = "index";
    //default parameters
    protected $params = "";

    public function __construct() {
        $url = $this->urlParse();
        //url[0] == public ??
        //url[1] -> Controller
        //url[2] -> Method
        //url[3...n] -> Parameters

        unset($url[0]);

        //check if requested controller exists
        if (file_exists(__DIR__ . '/../Controller/' . ucfirst($url[1]) . 'Controller.php')) {
            //
            $this->controller = ucfirst($url[1]) . "Controller";
            unset($url[1]);
        }
        $class = "\\App\Controller\\" . $this->controller;

        //create new instance of controller class
        $this->controller = new $class();

        //check if the requested method exists
        if (isset($url[2])) {
            if (method_exists($this->controller, $url[2])) {
                $this->method = $url[2];
                unset($url[2]);
            }
        }

        //if the url has any array elements left - define them as a parameters
        $this->params = $url ? array_values($url) : [];
        call_user_func_array([$this->controller, $this->method], $this->params);
    }

    public function urlParse() {
        //parse url paramater from request to an array
        //remove trailing slash
        //sanitize url
        //explode by slash
        if (isset($_GET['url'])) {
            return explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
        }
    }

}
