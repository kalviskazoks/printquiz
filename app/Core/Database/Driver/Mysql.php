<?php

namespace App\Core\Database\Driver;

use mysqli;
use App\Core\Database\Database;

/**
 * Description of mysqlDatabase
 *
 * @author kalvis
 * Implements common database methods by using mysqli class
 */
class Mysql implements Database {

    private $servername;
    private $database;
    private $user;
    private $password;
    private $connection;

    public function __construct($servername = "localhost", $database = "printful_base", $user = "printful", $password = "__!pr1N7FUL") {
        $this->servername = $servername;
        $this->database = $database;
        $this->user = $user;
        $this->password = $password;
    }

    public function getConnection() {
        $this->connection = new mysqli($this->servername, $this->user, $this->password, $this->database);
        if ($this->connection->connect_error) {
            die("Connection failed: " . $this->connection->connect_error);
        }
    }

    public function testConnection() {
        $this->getConnection();
        if ($this->connection) {
            $this->closeConnection();
            return true;
        }
        $this->closeConnection();
        return false;
    }

    public function closeConnection() {
        $this->connection->close();
    }

    public function query($sql) {
        $this->getConnection();
        if (!$this->connection) {
            die("Connection to database is not initialized!");
        }
        $result = $this->connection->query($sql);

        if (!$result) {
            $caller = debug_backtrace();
            $error = "SQL Error on " . $caller[1]['file'] . " row " . $caller[1]['line']
                    . "</br>ERRNO " . $this->connection->errno . ", ERROR: " . $this->connection->error;
            $this->closeConnection();
            die($error);
        }
        $this->closeConnection();
        return $result;
    }

    public function fetch($sql) {

        $this->getConnection();
        if (!$this->connection) {
            die("Connection to database is not initialized!");
        }
        $result = $this->connection->query($sql);

        if (!$result) {
            $caller = debug_backtrace();
            $error = "SQL Error on " . $caller[1]['file'] . " row " . $caller[1]['line']
                    . "</br>ERRNO " . $this->connection->errno . ", ERROR: " . $this->connection->error;
            $this->closeConnection();
            die($error);
        }

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_object()) {
                $this->closeConnection();
                return $row;
            }
        }
        $this->closeConnection();
        return null;
    }

    public function fetchAll($sql) {

        $this->getConnection();
        if (!$this->connection) {
            die("Connection to database is not initialized!");
        }
        $result = $this->connection->query($sql);

        if (!$result) {
            $caller = debug_backtrace();
            $error = "SQL Error on " . $caller[1]['file'] . " row " . $caller[1]['line']
                    . "</br>ERRNO " . $this->connection->errno . ", ERROR: " . $this->connection->error;
            $this->closeConnection();
            die($error);
        }

        $arr = [];
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_object()) {
                $arr[] = $row;
            }
        }
        $this->closeConnection();
        return $arr;
    }

    public function insert($table, $data) {
        $this->getConnection();
        if (!$this->connection) {
            die("Connection to database is not initialized!");
        }
        $data_e = array();
        foreach ($data as $key => $value) {
            $val = $this->connection->escape_string($value);
            if (is_string($val)) {
                $val = '"' . $val . '"';
            }
            $data_e["`" . $key . "`"] = $val;
        }

        $sql = "INSERT INTO `$table` (" . trim(implode(", ", array_keys($data_e))) . ") VALUES (" . trim(implode(", ", array_values($data_e))) . ")";
        $result = $this->connection->query($sql);

        if (!$result) {
            $caller = debug_backtrace();
            $error = "SQL Error on " . $caller[1]['file'] . " row " . $caller[1]['line']
                    . "</br>ERRNO " . $this->connection->errno . ", ERROR: " . $this->connection->error;
            $this->closeConnection();
            die($error);
        }

        $id = $this->connection->insert_id;
        $this->closeConnection();
        return $id;
    }

}
