<?php

namespace App\Core\Database;

/**
 * Description of DatabaseInterface
 * Defines Database interface and commonly used functions
 *
 * @author kalvis
 */
interface Database {

    public function getConnection();

    public function closeConnection();

    public function testConnection();

    public function query($sql);

    public function fetchAll($sql);

    public function fetch($sql);

    public function insert($table, $data);
}
