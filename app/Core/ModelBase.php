<?php

namespace App\Core;
use App\Core\Database\Driver\Mysql;

/**
 * Description of ModelBase
 *
 * @author kalvis
 * ModelBase class
 */
class ModelBase {
    
    protected $id;

    public function __construct() {
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }

}
