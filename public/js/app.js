$("#home_form").submit(function (e) {
    var form_data = $('#home_form').serialize();
    $.ajax({
        type: 'post',
        url: '/home',
        data: form_data,
        success: function (data) {
            if (data) {
                if (data.next) {
                    requestQuiz();
                } else {
                    $(".form-content").empty();
                    $(".form-content").html(data.form);
                }
            } else {
                console.log('No data received...');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus);
            if (errorThrown) {
                console.log(errorThrown);
            }
            return false;
        }
    });
    e.preventDefault();
});

function requestQuiz() {
    $.ajax({
        type: 'get',
        url: '\quiz',
        success: function (data) {
            if (data.container) {
                $(".container").empty();
                $(".container").html(data.container);
            } else {
                alert('No data received...');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus);
            if (errorThrown) {
                console.log(errorThrown);
            }
            return false;
        }
    });
}

$("#quiz_form").submit(function (e) {
    var form_data = $('#quiz_form').serialize();
    $.ajax({
        type: 'post',
        url: '/quiz',
        data: form_data,
        success: function (data) {
            if (data.next) {
                requestResults();
            } else {
                if (data.get) {
                    requestQuiz();
                } else {
                    $(".form-content").empty();
                    $(".form-content").html(data.form);
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus);
            if (errorThrown) {
                console.log(errorThrown);
            }
            return false;
        }
    });
    return false;
    e.preventDefault();
});

function requestResults() {
    $.ajax({
        type: 'get',
        url: '\results',
        success: function (data) {
            if (data) {
                $(".container").empty();
                $(".container").html(data.container);
            } else {
                alert('No data received...');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus);
            if (errorThrown) {
                console.log(errorThrown);
            }
            return false;
        }
    });
}