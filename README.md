# PrintQuiz
### Technical task v2

---
Requirements for the environment:

* PHP 7.0.33
* Apache/2.4.25
* MySQL Ver 15.1 Distrib 10.1.26-MariaDB
* Composer version 1.8.0

**WriteMe.md** also supports GitHub-style syntax highlighting for numerous languages, like so:

---
### Setup
1) Download project dependencies and update Composer's autoloader: 
```
composer update
composer dump-autoload -o
```